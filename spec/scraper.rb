require 'httparty'
require 'nokogiri'
require 'byebug'
require 'csv'
class Scraper

  HEADERS = %w{Year Mileage Engine_Capacity Fuel_Type Price}
  @@cars = Array.new
  def initialize(url)
    @url = url
    @num = 1
  end
  def get_all_data

    page = scrape_page(@url)
    last_page = set_last_page(page)
    loop_pages(last_page)
  end
  def set_last_page(page)
    begin
      page.css('ul.om-pager').last.css('span.page').text.gsub("...",' ').split(' ')[-1].to_i
    rescue
       page = 0
    end
  end
  def scrape_page(url)
    begin
    unparsed_page = HTTParty.get(url)
    Nokogiri::HTML(unparsed_page)
    rescue
       "I cant create Nokogiri::HTML::Document"
    end
  end
  def loop_pages(last_page)
    while @num <= last_page
      handle_next(last_page)
      @num+=1
    end
    if last_page != 0
      csv_generator
    end

  end
  def value
    @@cars
  end
  def handle_next(last_page)
    if last_page != 0
      url = "#{@url}?page=#{@num}"
      cars = scrape_page(url)
      car = cars.css('article.adListingItem')
      if(car != nil)
          build_cars(car)
      end
    end

  end
  def build_cars(cars)
   # puts "#{cars}"

    cars.each do |car|
      rok = car.css("li[data-code='year']").text.gsub("\n",' ').squeeze(' ').to_i
      przebieg =  car.css("li[data-code='mileage']").text.split.join(' ')
      pojemnosc = car.css("li[data-code='engine_capacity']").text.split.join(' ')
      paliwo = car.css("li[data-code='fuel_type']").text.split.join(' ')
      cena = car.css('span.offer-price__number').text.split.join(' ')
      car = [rok, przebieg, pojemnosc,paliwo, cena]
      @@cars << car
    end
  end

  def csv_generator
    CSV.open("./files/audi_a6_file.csv", 'wb') do |csv|
      csv<<HEADERS
      @@cars.each do |column|
        csv << column
      end
    end
    "Wygenerowano plik csv strony #{@url} wraz z jego podstronami."
  end

end

#scraper = Scraper.new("https://adsasdkasda.herokuapp.com/")
#scraper.get_all_data
