require 'rspec'
require_relative 'scraper'

RSpec.describe Scraper do

  describe  '#set_last_page' do
    subject{described_class.allocate}
    it 'returns number of last page' do
      parsed_page = subject.scrape_page("https://asdasfdsdnjfs.herokuapp.com/")
      expect(subject.set_last_page(parsed_page)).to be_kind_of(Integer)
    end
    it 'does return 0 as number of last page' do
      parsed_page = subject.scrape_page("https://adsasdkasda.herokuapp.com/")
      expect(subject.set_last_page(parsed_page)).to be_kind_of(Integer)
    end
  end
  describe '#scrape_page' do
    context 'when url begins with http or https'  do
      subject{described_class.allocate}
      it 'returns a Nokogiri::HTML::Document' do
        expect(subject.scrape_page("https://asdasfdsdnjfs.herokuapp.com/")). to be_a(Nokogiri::HTML::Document)
      end
      it 'does not return a Nokogiri::HTML::Document' do
        expect(subject.scrape_page("file:///home/kamil/Desktop/Ruby/d6/scraper/spec/site1/home.html")). to_not be_a(Nokogiri::HTML::Document)
      end
    end
  end
  describe '#loop_pages' do
    car = Scraper.new("https://asdasfdsdnjfs.herokuapp.com/")
    car1 = Scraper.new("https://adsasdkasda.herokuapp.com/")
    context 'when number of current page <= number of last page' do
      it 'calls method handle_text (number of last_page) times ' do
        expect(car).to receive(:handle_next).exactly(6).times
        car.loop_pages(6)
      end
      it 'does not call method handle_text' do

        expect(car1).to receive(:handle_next).exactly(0).times
        car1.loop_pages(0)
      end
      it 'calls method csv_generator once if number of current page = number of last page ' do
        expect(car).to receive(:csv_generator).once
        car.loop_pages(6)
      end
      it 'does not call method csv_generator' do
        expect(car1).to receive(:csv_generator).exactly(0).times
        car1.loop_pages(0)
      end
    end

  end
  describe '#handle_next' do
    car = Scraper.new("https://asdasfdsdnjfs.herokuapp.com/")
    car1 = Scraper.new("https://adsasdkasda.herokuapp.com/")
    it 'calls method build_cars once' do
      expect(car).to receive(:build_cars)
      car.handle_next(6)
    end
    it 'does not call method build_cars' do
      expect(car1).to_not receive(:build_cars)
      car1.handle_next(0)
    end
  end
  describe '#build_cars' do
    car = Scraper.new("https://asdasfdsdnjfs.herokuapp.com/")
    car1 = Scraper.new("https://adsasdkasda.herokuapp.com/")
    it 'does not fill the array ' do
      car1.get_all_data
      expect(car1.value).to be_empty
    end
    it 'fills in the array' do
      car.get_all_data
      expect(car.value).to_not be_empty
    end
  end

  describe '#csv_generator' do
    it 'does not generate csv file' do
      car = Scraper.new("https://adsasdkasda.herokuapp.com/")
      expect(car.loop_pages(0)). to_not eq("Wygenerowano plik csv strony https://adsasdkasda.herokuapp.com/ wraz z jego podstronami.")
    end
    it 'generates csv file' do
      car = Scraper.new("https://asdasfdsdnjfs.herokuapp.com/")
      car.get_all_data
      expect(car.loop_pages(6)). to eq("Wygenerowano plik csv strony https://asdasfdsdnjfs.herokuapp.com/ wraz z jego podstronami.")
    end

  end
  end
